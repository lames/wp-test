@extends('layouts.app')

@section('content')
  <div class="container">
    @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
    @endwhile
  </div>
  @if($slider)
    <div class="swiper-container">
      <div class="swiper-wrapper">
        @foreach($slider as $slide)
          <div class="swiper-slide {{$slide['dark_image'] === true? "dark":""}}" style="background-image: url('{{$slide['images']['url']}}')">
            <div class="container">
              <div class="splash-content">
                <header class="title">
                  <h1>{{$slide['heading']}}</h1>
                </header>
                <div class="splash-divider"></div>
                <div class="content">{!! $slide['text'] !!}
                </div>
                <div class="button primary">
                  <a
                    href="{{$slide['link']}}"
                    title="Buy Tickets" class="btn">{{$slide['link_text']}}</a>
                </div>
                <div class="button secondary">
                  <a href="{{$slide['link2']}}" title="Learn More" class="btn">{{$slide['link2_text']}}</a>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <div class="swiper-pagination"></div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
    </div>
  @endif
@endsection
