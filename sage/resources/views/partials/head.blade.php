<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" id="google_fonts-css" href="https://fonts.googleapis.com/css?family=PT+Serif%3A400%2C400italic%7CMontserrat%3A400%2C700%7COswald&amp;ver=5.3.2" type="text/css" media="all">
  @php wp_head() @endphp
</head>
