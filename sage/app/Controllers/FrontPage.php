<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function slider()
    {
        return get_field('slider');
    }

}
